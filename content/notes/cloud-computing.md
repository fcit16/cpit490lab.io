---
title: "Cloud Computing"
date: 2018-09-12T07:25:35+03:00
draft: false
---
## Week 4: Cloud Computing: Getting Started

### Infrastructure-as-a-Service (IaaS) Cloud Service Providers
- This class is a provider agnostic. You may pick any provider you prefer as long as you're using it at no cost (within a free plan or trial).
- There're many cloud service providers that offer free trials or student/education plans. Examples:
  - [Microsoft Azure](https://azure.microsoft.com/en-us/) has a free plan for [students with $100 credit](https://azure.microsoft.com/en-us/free/students/).
  - [AWS](https://aws.amazon.com/) has an education program called [AWS Educate](https://aws.amazon.com/education/awseducate/), and you can get extra [free credit using GitHub's student developer pack](https://education.github.com/pack).
  - [Digital Ocean](http://digitalocean.com/) and you can get extra [free credit using GitHub's student developer pack](https://education.github.com/pack).
  - [Google Cloud Platform](https://cloud.google.com/) has a [12 months $300 free trial](https://cloud.google.com/free/).
- Microsoft Azure is the recommended provider for students because it requires no credit card for verifying your account.
  - Signing up for an Azure account:
     - [Create your Microsoft Azure free student account](https://azure.microsoft.com/en-us/free/students/).
     - You do not need to provide any credit card information when you sign up using your .edu email address.
     - You will receive $100 for your MS Azure student account. This should be more than enough for this course.

### Creating a New Virtual Machine (VM) Instance in the Cloud
- First, review the pricing list of your provider. Prices vary by resources (e.g., CPU and memory to name a few), region (the data center where your VM is located), and availability (e.g., replication).
- Most providers offer two ways create a new virtual machine instance: using their dashboard/portal web interface and a special command line interface.
- Creating a new VM instance using the dashboard web interface:
  - __Azure__: See [Quickstart: Create a Linux virtual machine in the Azure portal](https://docs.microsoft.com/en-us/azure/virtual-machines/linux/quick-create-portal)
  - __AWS__: See [Launch a Linux Virtual Machine with Amazon EC2](https://aws.amazon.com/getting-started/tutorials/launch-a-virtual-machine/?trk=gs_card)
  - __Digital Ocean__: See [How to Create a Droplet from the DigitalOcean Control Panel](https://www.digitalocean.com/docs/droplets/how-to/create/)
  - __Google Cloud Platform__: See [Quickstart Using a Linux VM ](https://cloud.google.com/compute/docs/quickstart-linux)
- You need to create ssh keys and add them to your VM when creating the VM using the Azure portal. See the


### Logging in to Your Virtual Machine VM Instance<a name="logging-in-to-your-virtual-machine-vm-instance"></a>
- To log in to your virtual machine (VM) instance, you need to use SSH.
- SSH is an encrypted connection protocol that allows secure sign-ins to a remote server.
- SSH connection can be established using passwords or public–key cryptography.
- Although SSH is a secure protocol, using passwords makes accessing your VM instance vulnerable to guessing or brute force attacks.
- A more secure approach of connecting to a remote server using SSH is by using public–key cryptography.
- Public–key cryptography involves using a public-private key pair, also known as SSH keys, to encrypt and decrypt login information.
- Your cloud provider stores the public key only inside your instance, and you store the private key.
- Anyone who possesses your private key can login to your instance or decrypt your login information, so it's important that you store your private keys in a secure place on your client machine.
- To add an extra layer of security to your SSH key, add a passphrase to your SSH key.
- To access your VM instance using SSH keys, you need to have on your client machine a terminal emulator and an SSH client.
- Below is how to SSH to your VM instance. Replace ip_or_domain with the fully qualified domain name (or IP address).
    
    ```shell
   $ ssh -i /home/you/.ssh/your_private_key root@ip_or_domain
    ```


### SSH Clients and Terminal/Console Emulators<a name="ssh-clients-and-terminal-console-emulators"></a>
- You need to establish an ssh connection from your client machine into a remote server.
- The most common SSH access method is through a command line SSH client (remote login program).
- You need a terminal/console emulator to use the SSH client on your local machine.
  - Windows:
      - [Cmder](http://cmder.net/): A portable console emulator for Windows.
      - [Hyper](https://hyper.is/): A terminal built on web technologies.
      - [Mintty](https://mintty.github.io): A terminal emulator for [Cygwin](http://cygwin.com/)
      - [Git for Windows ](https://gitforwindows.org/): a BASH emulation with Git support using the console or GUI.
  - macOS
      - Terminal: Apple's default terminal emulator.
      - [iTerm2](https://www.iterm2.com/):  An alternative to the default Terminal app
  - Linux
      - [xterm](https://invisible-island.net/xterm/): The standard terminal emulator on most linux distributions.


### Generating New SSH Key Pair<a name="generating-new-ssh-pey-pair"></a>
The following steps assume that you are using a BASH shell on your client machine using the previous step.

1. Create SSH key pair with OpenSSH using RSA encryption and a bit length of 4096
    
    ```bash
 $  ssh-keygen -t rsa -b 4096 -C "provide comment to label the key (e.g. my azure keys)"
```

2. You will be prompted to enter a path to save the keys and a passphrase
      
    ```bash
Generating public/private rsa key pair.
Enter file in which to save the key (/home/you/.ssh/id_rsa): /home/you/.ssh/azure_key
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/you/.ssh/azure_key.
Your public key has been saved in /home/you/.ssh/azure_key.pub.
The key fingerprint is:
a7:83:cc:76:83:21:1f:2a:ec:a4:04:11:e5:e3:b1:e8 my azure keys
The key's randomart image is:
+--[ RSA 4096]----+
|...              |
| o               |
|. +              |
| + +             |
|o o . o S .      |
|o.   * = o       |
| E+ . B =        |
|.+ . . . o       |
|. .              |
+-----------------+
```
This will generate two keys: a private key `/home/you/.ssh/azure_key` and a public key `/home/you/.ssh/azure_key.pub`.
The passphrase you entered will add an extra layer of security, so you will be asked to enter this passphrase every time you use your SSH keys. 
- To avoid typing your private key file passphrase multiple times, you can use `ssh-agent` to securely save your private key file passphrase, so you don't have to reenter it. To start `ssh-agent`, run:
    
    ```bash
$ eval "$(ssh-agent -s)"
```
4. Add the private key to `ssh-agent` using the command `ssh-add`:
    
    ```bash
$ ssh-add ~/.ssh/id_rsa
```

### Adding your new SSH key to your cloud provider account<a name="adding-your-new-ssh-key-to-your-cloud-provider-account"></a>
To configure your VM instance to use your new (or existing) SSH key, you'll also need to add your **public key** to your cloud provider account (e.g., Azure web portal/console).

1. Copy your public key using a text editor or use the pasteboard copy tool, `pbcopy`:
    
    ```bash
    pbcopy < ~/.ssh/azure_key.pub
    ```
2. Import your public key into the VM instance when creating it using the web console interface:
  - [Azure](https://docs.microsoft.com/en-us/azure/virtual-machines/linux/quick-create-portal#create-virtual-machine)
  - [AWS](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html#how-to-generate-your-own-key-and-import-it-to-aws)
  - [Digital Ocean](https://www.digitalocean.com/docs/droplets/how-to/add-ssh-keys/to-account/)
  - [Google Cloud Platform](https://cloud.google.com/compute/docs/instances/adding-removing-ssh-keys)

**Note:** You may add the public key into an existing VM instance. Please refer to the documentation of your cloud provider.