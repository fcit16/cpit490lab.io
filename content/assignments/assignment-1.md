---
title: "Assignment 1"
date: 2018-08-25T22:54:32+03:00
draft: false
---

# Assignment 1 <span class="tag is-light assignment-due-date">Due: Tuesday 18/9/2018 at 1:00PM</span>

Follow the steps in [User and Software Management](/notes/user-software-management/) and [The Filesystem](/notes/filesystem) and execute all the commands in these two note articles.

Submit a PDF file that contains screenshots of your system showing the execution of the commands and the output. The PDF file should be submitted on Slack in private as a direct message (DM) by the due date.
