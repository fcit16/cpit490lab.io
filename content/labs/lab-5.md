---
title: "Lab 5"
date: 2018-09-01T16:39:04+03:00
draft: false
---



# Lab 5

> Install and configure a distributed software system that uses the _LAMP_ stack (Linux, Apache web server, MySQL/MariaDB, and PHP).

<span class="tag is-info is-medium">Due on Sunday 28/10/2018 at 1:00PM</span>

You will install and configure a web application called _WordPress_, a popular Content Management System (CMS), on two different VM instances where the first one hosts the web server and the second one hosts the database server.

<iframe width="560" height="315" src="https://www.youtube.com/embed/TbSXUpVjZCo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

### Setup/Prerequisites
- This lab builds upon the work in the [previous lab](labs/lab-4). Please make sure that you finish it before attempting this lab.
- This lab assumes that you have an account with a cloud provider (e.g., _Microsoft Azure_). See [this lecture note on how to create an account with _Microsoft Azure_ and other cloud providers](/notes/cloud-computing).
- Create SSH key pair using `ssh-keygen` and upload your public key into the cloud.
- You need to create two VM instances running CentOS 7 and access them using SSH.
- You also need to create two external/data disks and attach them into your instances.

### Note
- Most cloud providers feature a web-based SSH client accessible from the dashboard/portal. This is in particular useful in situations when you're behind a firewall and the default SSH port 22 is blocked.
- Below is a list of new commands you will be using in these lab exercises. Make sure you read the manual of each one using the command `man`.
  - `lsblk` -- list block devices
  - `mkfs` -- build a Linux filesystem
  - `file` -- determine file type

### The Steps
#### Step One — Create VM instances, attach data disks, and log in using SSH

1. Create two Linux (CentOS 7) VM instances with two additional disks (5-10 GB each) and attach them into each instance.
  - The first instance is the web server instance and will run Apache, php, and wordpress. In the following steps, we will refer to it as __web server instance__.
  - The second instance is the database server and will run _MariaDB_. In the following steps, we will refer to it as __db instance__.
2. Log in to your VM instances using SSH

     ```bash
       $ ssh -i /path/to/your/private/key/file azureuser@public-ip-or-DNS-name
     ```
    - Type the _passphrase_ you entered when generating the SSH key pair.
    - Now you're logged in to your remote VM instance.
    - If you're not logged in to your VM as a _root_, add your user name to the _wheel_ group.
3. If you’re not familiar with VIM, you may find the _nano_ editor easier to work with. Install it on both instances: `sudo yum install nano`.

#### Step Two — Mount the external disk
1. Make sure that your external storage devices are attached to the instance from the dashboard of your cloud provider.
2. Find the device name of the external storage/disk you created and attached to each instance by running the command `lsblk` to list all available devices:
      
     ```bash
$ lsblk 
NAME    MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
xvda    202:0    0  40G  0 disk 
`-xvda1 202:1    0  40G  0 part /
xvdf    202:80   0   8G  0 disk
     ```

3. The above output shows that the default (OS) disk is _xvda_ with a partition called _xvda1_ mounted on **/**. The additional/external disk is called **xvdf** and it is not mounted on the filesystem yet.
4. Next, we need to determine whether the disk has been formatted with a filesystem or not. In this example, the disk **device_name** is called **xvdf**. In the following steps, replace **device_name** with the name of your disk device. To determine whether your disk has a filesystem or not, run the following command:
       
     ```bash
      $ sudo file -s /dev/device_name
     ```
  - You should see the following output on a brand new/not formatted disk:
     
        ```
        /dev/xvdf: data
        ```
  - You should see an output similar to the following on a disk that has already a file system:
        
        ```
            /dev/xvdf: Linux rev 1.0 ext4 filesystem data, UUID=1701d228-e1bd-4094-a14c-12345EXAMPLE (needs journal recovery) (extents) (large files) (huge files)
        ```
5. If you have an empty disk, then you need to format the disk with a file system such as ext4.
  - Format the disk with a single ext4 file system:
<article class="message is-danger">
  <div class="message-body">
  <strong>Warning</strong>
  <p>The following command assumes that you're going to mount and format an empty disk. If your disk has data, then skip this step. Otherwise, you'll format the disk and delete the existing data.</p>
  </div>
</article>
       
        ```bash
       $ sudo mkfs -t ext4 /dev/device_name
        ```
  - You should see an output like the following:
        
        ```
        mke2fs 1.42.9 (28-Dec-2013)
        Filesystem label=
        OS type: Linux
        Block size=4096 (log=2)
        Fragment size=4096 (log=2)
        Stride=0 blocks, Stripe width=0 blocks
        524288 inodes, 2097152 blocks
        104857 blocks (5.00%) reserved for the super user
        First data block=0
        Maximum filesystem blocks=2147483648
        64 block groups
        32768 blocks per group, 32768 fragments per group
        8192 inodes per group
        Superblock backups stored on blocks: 
	    32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632

        Allocating group tables: done                            
        Writing inode tables: done                            
        Creating journal (32768 blocks): done
        Writing superblocks and filesystem accounting information: done
        ```

6. Create a mounting point using:
     
     ```bash
     $ sudo mkdir -p /mnt/disk-1
     ```
7. Mount the disk
     
     ```bash
     $ sudo mount /dev/device_name /mnt/disk-1
     ```
8. Mount the disk every time you reboot your instance:
  - You need to add an entry for the device to the `/etc/fstab` file, but before that it's recommended that you backup the original file before edit it:

     ```bash
     $ sudo cp /etc/fstab /etc/fstab.orig
     ```
  - Open the `/etc/fstab` file using any text editor, such as nano.
  - Add a new line to the end of the file after replacing _device-name_ with the name of the disk (e.g., xvdf).

     ```bash
     /dev/device-name /mnt/disk-1 ext4 defaults, nofail 0 2
     ```

    - Replace device-name with the name of the disk (e.g., xvdf).
  - Save the file and exit your text editor
9. Repeat the above steps on the second instance

#### Step Three — Install MariaDB

1. On the __db instance__ install __MariaDB__.
  - MariaDB is not available in the default CentOS 7 yum repositories, so we need to add it manually as a third-party repository.
  - Add MariaDB to the default CentOS repositories.
    - Create a file `/etc/yum.repos.d/MariaDB.repo` with the following content:
        
        ```
    [mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.1/centos7-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
    ```
    - Next, install MariaDB using `$ sudo yum install MariaDB-server MariaDB-client`
2. Start MariaDB with: `$ sudo systemctl start mariadb`


#### Step Four - Change MariaDB's Data Directory to a new location
We need to change the default data directory of MariaDB into a directory on the external disk we mounted in the previous step:

1. Stop MariaDB: `$ sudo systemctl stop mariadb`
2. Ensure MariaDB has stopped `$ sudo systemctl status mariadb`. You should see in the output that the status is _"MariaDB server is down"_
3. Create a new directory: `$ sudo mkdir /mnt/disk-1/db-data`
4. Change the owner of that directory into the default user that runs mariadb:
   
      ```bash
   $ sudo chown mysql:mysql /mnt/disk-1/db-data
      ```
5. Edit MariaDB's configuration files to use the new location:
   - Edit the server config file at ``

   ```bash
   $ sudo nano /etc/my.cnf
   ```
  - Find the line that begins with `[mysqld]` and add the following:
         
       ```
    . . .
   [mysqld]
   datadir=/mnt/disk-1/db-data/mysql/
   socket=/mnt/disk-1/db-data/mysql/mysql.sock
   . . .
   
       ```
  - Edit the client config file at `/etc/my.cnf.d/client.cnf`
      
      ```bash
      $ sudo nano /etc/my.cnf.d/client.cnf
      ```
  - Find the line that begins with `[client] and add the following:
     
     ```
  [client]
  port=3306
  socket=/mnt/disk-1/db-data/mysql/mysql.sock
     ```
6. Copy the default data directory into the new location
   ```bash
   $ sudo cp -av /var/lib/mysql /mnt/disk-1/db-data
   ```
   - The -a is for the "archive mode", which recursively copies the content and preserves the permissions and other file properties, and the -v flag provides verbose output.
7. Set SELinux Security context to the data directory
   ```bash
   $ sudo semanage fcontext -a -t mysqld_db_t "/mnt/disk-1/db-data/mysql(/.*)?"
   $ sudo restorecon -R /mnt/disk-1/db-data/mysql
   ```
7. Next start MariaDB using `sudo systemctl start mariadb`
8. Make sure that the new data directory is being used: 
  - Start the MariaDB/MySQL client using: 
     ```bash
     $ mysql -u root -p
     ```
     - Leave the password blank and hit Enter
     - Run the following SQL statement: `select @@datadir;`
     - You should see the following output:
                
              ```
     MariaDB [(none)]> SELECT @@datadir;
+----------------------------+
| @@datadir                  |
+----------------------------+
| /mnt/disk-1/db-data/mysql/ |
+----------------------------+
1 row in set (0.00 sec)
              ```
      - Type `exit` and hit Enter

#### Step Five — Install the Apache HTTP Server (httpd) 
Install the _apache  HTTP Server (httpd)_ web server on the __web server instance__.

1. Install Apache using yum:
     
     ```bash
  $ sudo yum -y install httpd
  ``` 
2. Create a new directory on the external disk you mounted previously and change its owner to the default user that runs apache:
      
      ```bash
     $ sudo mkdir /mnt/disk-1/www/
     $ sudo chown apache:apache /mnt/disk-1/www/
      ```
3. Edit the configuration at `/etc/httpd/conf/httpd.conf` using a text editor:
    - `sudo nano /etc/httpd/conf/httpd.conf`
    - Change the Path to _the root directory_ of your web server. The default value is `/var/www` so change it to `/mnt/disk-1/www`.
          
          ```
            DocumentRoot "/mnt/disk-1/www"
            #
            # Relax access to content within /var/www.
            #
            <Directory "/mnt/disk-1/www">
            AllowOverride None
            # Allow open access:
            Require all granted
            </Directory>

            # Further relax access to the default document root:
            <Directory "/mnt/disk-1/www">

            . . .
          ```

4. If you have SELinux enabled, then do the following:
   
      ```bash
    $ sudo chcon -Rt httpd_sys_content_t /mnt/disk-1/www/
    $ sudo setsebool -P httpd_can_network_connect on
       ```
5. Start _the Apache  HTTP Server _
 ```bash
   $ sudo systemctl start httpd
 ```


#### Step Six — Install PHP
We need to install PHP version 7.2 or greater as per the [system requirement of WordPress](https://wordpress.org/about/requirements/). PHP 7 is not included in the default CentOS repositories, so we need to add it manually using the Remi and EPEL repository packages:

1. Install and enable EPEL and Remi repository to your CentOS 7
   
      ```bash
$ sudo yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
$ sudo yum -y install http://rpms.remirepo.net/enterprise/remi-release-7.rpm
      ```
2. Install and configure yum-utils

      ```bash
   $ sudo yum -y install yum-utils
   $ sudo sudo yum-config-manager --enable remi-php72
      ```
3. Install PHP 7.2 and the required modules
   
      ```bash
$ sudo yum -y install php php-mysql php-gd 
      ```

4. Test it by executing the command: `$ php -v`
      
      ```
PHP 7.2.11 (cli) (built: Oct 10 2018 10:00:29) ( NTS )
Copyright (c) 1997-2018 The PHP Group
Zend Engine v3.2.0, Copyright (c) 1998-2018 Zend Technologies
       ``` 

#### Step Seven — Install WordPress

1. Download WordPress and extract it at your home directory:
  - Download the latest WordPress release and extract it
         
         ```bash
         $ curl -LO http://wordpress.org/latest.tar.gz
         $ tar xvzf latest.tar.gz 
         ```
  - Copy the extracted directory into Apache' document root
     
         ```bash
   sudo cp -av wordpress/* /mnt/disk-1/www/
         ```
  - Create a directory for WordPress to store uploaded files
     
         ```bash
    $ sudo mkdir /mnt/disk-1/www/wp-content/uploads
         ```
  - Change the owner of the WordPress directory to the apache web server's owner
     
         ```bash
    $ sudo chown -R apache:apache /mnt/disk-1/www/
         ```

#### Step Eight - Configure WordPress
1. Use the sample config file

      ```bash
      $ cd /mnt/disk-1/www/
      $ sudo cp -a wp-config-sample.php wp-config.php
      ```
2. Open the default config file `wp-config.php` in your text editor and make the following changes:
  - `sudo nano wp-config.php`
  - Change the database name, database username, and password. We also need to change the default hostname from localhost into the _private ip address of your_ __db instance__ (assuming both instances are in the same region and private network).
            
         ```sql
         // ** MySQL settings - You can get this info from your web host ** //
         /** The name of the database for WordPress */
         define('DB_NAME', 'wpdb');

         /** MySQL database username */
         define('DB_USER', 'wpuser');

         /** MySQL database password */
         define('DB_PASSWORD', 'wppassword');
         /** MySQL hostname */
         define('DB_HOST', 'put-the-private-ip-of-db-instance-here');
         ```
  <article class="message is-info">
  <div class="message-body">
  <strong>Note</strong>
  <p>
  When we have a web server and a database server, it's more secure to communicate over a private network, because you don't need to open any public ports.
  </p>
  </div>
  </article>


3. Create a MySQL database and user for WordPress using the same values you entered in the previous step.
  - Log in to your __db instance__ and start the MariaDB/MySQL client using: 
     ```bash
     $ mysql -u root -p
     ```
     - Leave the password blank and hit Enter
     - Run the following SQL statement: 
          
          ```sql
          CREATE DATABASE wpdb;
          USE wpdb;
          ```
  - Create a new user: 
       
          ```sql
    CREATE USER wpuser@localhost IDENTIFIED BY 'wppassword';
          ```
    - Grant privileges to the new user. Obtain the private IP address of the web server instance before executing the command below. Replace ip-address with the IP address of the web server instance. 
              
        ```sql
        GRANT ALL PRIVILEGES ON wpdb.* TO wpuser@private-ip-web-server-instance IDENTIFIED BY 'wppassword';
        FLUSH PRIVILEGES;
        ```
  - Exit the MySQL prompt using: `exit`

#### Step Nine — Complete the Installation Through the Web Interface

1. From your web browser, visit the WordPress dashboard using the public ip address of your __web server instance__ `http://server_domain_name_or_public_IP/wp-admin/`.
2. Complete the installation through the web interface.
![](/images/week-6/wp-install.png)
3. Log in to the admin dashboard, write your first blog post, and publish it.
4. Access the website to show the blog post
![](/images/week-6/wp-website.png)
3. Clean up resources and delete the VM instance.

## Submission
Submit your answers with screenshots showing the commands you executed as a PDF file by the due date.

