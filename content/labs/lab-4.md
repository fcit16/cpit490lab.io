---
title: "Lab 4"
date: 2018-09-01T16:39:02+03:00
draft: false
---

# Lab 4

> Generate SSH key pair, create a VM instance, log in to the VM instance, install a web server, add a simple web page, view the web page in action, and destroy the instance.

<span class="tag is-info is-medium">Due on Monday 15/10/2018 at 1:00PM</span>

<iframe width="560" height="315" src="https://www.youtube.com/embed/g7tOVZ8HLxA?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

### Setup
- This lab assumes that you're using Bash Shell on both client and server.
  - Both Linux and macOS include a bash shell. For Windows, see [this lecture note on how to install a Bash shell on Windows](/notes/cloud-computing/#ssh-clients-and-terminal-console-emulators).
- This lab assumes that you have an account with a cloud provider (e.g., _Microsoft Azure_), see [this lecture note on how to create an account with _Microsoft Azure_ and other cloud providers](/notes/cloud-computing).
- You need to have a text editor (e.g., nano or vi) and be familiar with using it.

### Notes
- Below is a list of commands you will be using in these lab exercises. Make sure you read the manual of each one using the command `man`.
  - `ssh-keygen`
  - `ssh`


### Exercises
1. Generate SSH key pair on your client machine
  - Use `ssh-keygen` to generate SSH key pair (public and private keys) using RSA encryption and a bit length of 4096. 
      
     ```bash
   $ ssh-keygen -t rsa -b 4096 -C "azure-key"
     ```
    - You will be prompted to enter a path to save the keys and a passphrase. You will need to enter the passphrase every time you use the generated private key.
2. Copy the generated public key and add it to your cloud provider account.
      ```bash
      pbcopy < /path/to/your/public/key/file/your-public-key.pub
      ```
3. Log into your cloud provider account, create a new VM instance, and add the public key you copied in the previous step. In the case of _Microsoft Azure_, you will do:
     - Log in to the Azure portal at [http://portal.azure.com](http://portal.azure.com).
     - Click on _"Virtual Machines"_ in the left-hand side bar of the Azure portal.
     - Click on **+ Add** 
     - In the **Basics tab**, you should have one subscription that is selected and then choose to _Create new_ under _Resource group_. In the pop-up, type any name, and then choose _OK_.
     - Under **Instance details**, type _myCentOSVM_ for the Virtual machine name, choose _East US_ for your Region, choose _CentOS 7_ for the image, and the cheapest VM type _(B1S 1vCPU 1GiB RAM)_.
     - Under **Administrator account**, select SSH public key, and paste the public key you copied in the previous step.
     - Under **Inbound port rules, Public inbound ports**, choose _"Allow selected ports"_ and then select _SSH (22)_ and _HTTP (80)_ from the drop-down menu.
     - Click on **Next: Disks >** and create a 30GiB standard disk (HDD).
     - Click **Next** and leave the default selections for **Networking**, **Management**, **Guest Config**, and **Tags**.
     - Click **Review + create**.
4. Connect to the VM instance using SSH
  - Click on the **Connect** button on the overview page for your VM.
  - Connect using one of the following methods:
    - You can copy and paste the command into your client shell to log in.
    - Use the public ip and user name you chose when creating your VM and run:
    
       ```bash
       $ ssh -i /path/to/your/private/key/file azureuser@public-ip-or-DNS-name
       ```
    - Type the _passphrase_ for your VM instance.
    - Now you're logged in to your remote VM instance.
    - If you're not logged in to your VM as a _root_, create a new user, and add it to the _wheel_ group.
5. Install the **nginx** web server on your VM.
  - _nginx_ is not included in the default CentOS repositories, so we need to add the _nginx_ repository as a 3rd party source manually.
  - Create a file named `sudo touch /etc/yum.repos.d/nginx.repo` with the following content

      ```bash
  [nginx]
  name=nginx repo
  baseurl=http://nginx.org/packages/centos/7/$basearch/
  gpgcheck=0
  enabled=1
     ```
  - Next install nginx using yum:
     
     ```bash
  $ sudo yum -y install nginx
  ``` 
6. Start _nginx_
 ```bash
   $ sudo systemctl start nginx
 ```
7. Open the public ip or domain of your VM in your browser.
  - You should see something like:
    
    ![](/images/week-5/nginx-welcome-page.png)
8. Add a custom HTML page. 
   - Open the config file at `/etc/nginx/conf.d/default.conf` in your text editor:
      
      ```bash
         $ sudo nano /etc/nginx/conf.d/default.conf
      ```
    - Change the Path to _the root directory_ of your web server. The default value is `/usr/share/nginx/html` so change it to `/data/www/`
       
       ```bash
       location / {
          root /data/www/;
       }
       ```
   - Create a data directory for our web pages.
      
      ```bash
      $ sudo mkdir -p /data/www/cpit490site
      ```
   - Create a simple **index.html** page using `$ sudo nano /data/www/cpit490site/index.html` with the following content:
   
      ```html
      <!DOCTYPE html>
      <html lang="en">
        <head>
            <title>CPIT-490</title>
        </head>
        <body>
            <h1>Welcome to CPIT-490 website</h1>
        </body>
      </html>
   ```
8. Change the owner, permissions, and add firewall rules
     - We need to change the owner (chown) of our data directory to the user who runs the nginx server, which is usually **nginx**. We also need to change the permission to **755** for directories and **644** for files. These are the recommended permission bits for files served by a web server.
     
        ```bash
        $ sudo chown -R nginx:nginx /data/www/
        $ sudo chmod -R 755 /data/www
        $ sudo chmod 644 /data/www/cpit490site/index.html
        ```
     - **Note:** The username used in the `chown` command should be the one which nginx runs with. You can check that by running the command: `ps -ef | grep nginx` and you should see that the first column of the _worker process_ shows **nginx** as the username. Alternatively, you can open the default config file at `/etc/nginx/nginx.conf` and you should see the value of the user directive is **nginx**.
     - Install the firewall manager, _firewalld_, enable it, and add rules to enable outbound HTTP traffic over port 80
         
         ```bash
         $ sudo yum -y install firewalld
         $ sudo systemctl unmask firewalld
         $ sudo systemctl enable firewalld
         $ sudo systemctl start firewalld
         $ sudo firewall-cmd --get-default-zone
         public
         $ sudo firewall-cmd --permanent --zone=public --add-service=http 
         $ sudo firewall-cmd --reload
         ```
9. Access the web page using the public IP address of your VM instance.
   - Obtain the public IP address or domain of your VM instance from the cloud provider's web portal.
   - Open your browser and visit **ip_address_or_domain/cpit490site**
   - If you see a _403_ error, try the following:
     - check if _SELinux_ is running:
       
       ```bash
        $ sudo getenforce
       ```
    - disable _SELinux_
       
       ```bash
       $ sudo setenforce Permissive
       ```
    - Try to access the web page again. If it works, then disable SELinux permanently:
       
       ```bash
       $ sudo chcon -Rt httpd_sys_content_t /data/www/
       ```
    - If it still does not work, run:
       
       ```bash
       $ sudo setsebool -P httpd_can_network_connect on
       ```
9. Clean up resources
  - Stop the nginx web server:
    
     ```bash
     $ sudo systemctl stop enginx
     ```
  - Delete VM instance


## Submission
Submit your answers with screenshots showing the commands you executed as a PDF file by the due date.
