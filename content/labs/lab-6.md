---
title: "Lab 6"
date: 2018-09-01T16:39:06+03:00
draft: false
---


# Lab 6

> Install and configure a virtual private network (VPN) server to restrict access to your cloud VM instances to VPN only.

<div class="tags">
  <span class="tag">Networking</span>
  <span class="tag">Security</span>
  <span class="tag">Cloud-Computing</span>
</div>

<span class="tag is-info is-medium">Due on Sunday 4/11/2018 at 1:00PM</span>

You will install and configure a secure VPN server called _OpenVPN_ to create a secure point-to-point connection and protect your VM instances.  This VPN Server serves as the gateway to our protected VM instances. We will limit access to our instances to VPN only and gain control to direct SSH access to our VM instances. We can also block/revoke access to our entire private network via our VPN Server.


<article class="message is-info">
  <div class="message-body">
  <strong>Note</strong>
  <p> Note: In addition to the cost of the computing resources such as VM instances, setting up a VPN server on the cloud has an additional cost for bandwidth overuses. For this reason, please refer to the pricing page of your cloud provider and be mindful of how much traffic your server is handling. 
</p>
  </div>
</article>

  <article class="message is-warning">
  <div class="message-body">
  <strong>Disclaimer</strong>
  <p> 
  The purpose of this lab is to secure your VM instances and protect access to your internal resources when using otherwise insecure or untrusted networks. This is a common practice in the IT industry.
  </p>
  <p>
  VPN has many other security and privacy applications some of which may violate local laws. It's your responsibility to abide by applicable laws and become educated. The instructor is not responsible for other uses that may violate any local laws.
</p>
  </div>
</article>

### Setup/Prerequisites
- This lab assumes that you have an account with a cloud provider (e.g., _Microsoft Azure_). See [this lecture note on how to create an account with _Microsoft Azure_ and other cloud providers](/notes/cloud-computing).
- Create SSH key pair using `ssh-keygen` and upload your public key into the cloud.
- You need to create a VM instance running CentOS 7 and access it using SSH.

### Note
- Most cloud providers feature a web-based SSH client accessible from the dashboard/portal. This is in particular useful in situations when you're behind a firewall and the default SSH port 22 is blocked.


### The Steps
#### Step One — Create VM instances and access it using SSH
1. Create a Linux (CentOS 7) VM instance.
2. Log in to your VM instances using SSH

     ```bash
       $ ssh -i /path/to/your/private/key/file azureuser@public-ip-or-DNS-name
     ```
    - Type the _passphrase_ you entered when generating the SSH key pair.
    - Now you're logged in to your remote VM instance.
    - If you're not logged in to your VM as a _root_, add your user name to the _wheel_ group.
3. In your cloud provider dashboard, go to the firewall settings and find the option that controls which ports are open to the public. Open the following inbound ports:
  - Protocol: UDP, Port: 1194 (for VPN)
  - Protocol: TCP, Port: 22 (for SSH)
4. If you’re not familiar with the VIM, you may find the _nano_ editor easier to work with. Install it on your instance using: `sudo yum install nano`.
5. Create a static IP address for your instance. This is important because we want the IP address of our VPN Server to remain static/unchanged. This IP address will be the Public IP Address of your VPN Server.


#### Step Two — Install OpenVPN
1. Install and enable the epel-repository
   - OpenVPN isn't available in the default CentOS repositories but it is available in the Extra Packages for Enterprise Linux (_EPEL_), so we need to install _EPEL_ as follows:
   
      ```bash
      $ sudo yum -y install epel-release
      ```
2. Install easy-rsa and firewalld
    - Easy RSA is a public key infrastructure management tool. We need it to set up an internal certificate authority (CA) for our VPN and create SSL key pairs to secure the VPN connection.
      
      ```bash
      $ sudo yum -y install easy-rsa
      ```
    - Install and enable _firewalld_
          
      ```bash
      $ sudo yum -y install firewalld
      $ sudo systemctl unmask firewalld
      $ sudo systemctl enable firewalld
      $ sudo systemctl start firewalld
      ```

3. Install the latest version of [openVPN](https://github.com/OpenVPN/openvpn)
      
      ```bash
      $ sudo yum -y install openvpn
      ```

#### Step Three - Generating Keys and Certificates
The first step in building an OpenVPN server is to establish a PKI (public key infrastructure). The PKI consists of:
- A separate certificate (also known as a public key) and private key for the server and each client that connects to the VPN server.
- A master Certificate Authority (CA) certificate and key which is used to sign each of the server and client certificates.
OpenVPN authentication uses a bidirectional authentication model based on certificates. That's it, the client must authenticate the server certificate and the server must authenticate the client certificate before mutual trust is established.
It's important to note that the the loss of the CA key destroys the security of the entire PKI.

##### The CA
1. Set up a CA PKI 
   
      ```bash
      $ mkdir ~/ca;
      $ cs ~/ca;
      $ sudo /usr/share/easy-rsa/3/easyrsa init-pki
      ```
      ```
      init-pki complete; you may now create a CA or requests.
      Your newly created PKI dir is: /home/centos/ca/pki
      ```

2. Build a root CA with an encrypted CA key
   - Build the CA. You will be asked for a PEM passphrase and a common name.
      
      ```bash
      $ sudo /usr/share/easy-rsa/3/easyrsa build-ca
      ```
      ```
      Generating a 2048 bit RSA private key
      ....+++
      ......+++
      writing new private key to '/home/centos/ca/pki/private/ca.key.nkob0wzC2z'
      Enter PEM pass phrase:
      Verifying - Enter PEM pass phrase:
      -----
      You are about to be asked to enter information that will be incorporated
      into your certificate request.
      What you are about to enter is what is called a Distinguished Name or a DN.
      There are quite a few fields but you can leave some blank
      For some fields there will be a default value,
      If you enter '.', the field will be left blank.
      -----
      Common Name (eg: your user, host, or server name) [Easy-RSA CA]:myvpn
      
      CA creation complete and you may now import and sign cert requests.
      Your new CA certificate file for publishing is at:
      /home/centos/ca/pki/ca.crt
      
      ```
     <article class="message is-info">
  <div class="message-body">
  <strong>Note</strong>
  <p>
  In a production environment and for security reasons, you should use the fully qualified domain name (FQDN) of your host as the common name for your CA.
  </p>
  </div>
  </article>
3. Generate the certificate revocation list
      
      ```bash
      $ sudo /usr/share/easy-rsa/3/easyrsa gen-crl
      ```
 

##### The server
1. Setup the server PKI. Configure a secondary PKI environment on the server at a separate directory:
     
     ```bash
     $ mkdir ~/server;
     $ cd server ~/server;
     $ sudo /usr/share/easy-rsa/3/easyrsa init-pki
     ```
     ```
     init-pki complete; you may now create a CA or requests.
     Your newly created PKI dir is: /home/centos/server/pki
     ```
2. Generate the server keypair and request
      
    ```bash
    $ sudo /usr/share/easy-rsa/3/easyrsa gen-req my-vpn-server-key nopass
    ```
    ```
    Generating a 2048 bit RSA private key
    ...............................+++
    ........................................................+++
    writing new private key to '/home/centos/server/pki/private/my-vpn-server-key.key.PdwAnpHkZD'
    -----
    You are about to be asked to enter information that will be incorporated
    into your certificate request.
    What you are about to enter is what is called a Distinguished Name or a DN.
    There are quite a few fields but you can leave some blank
    For some fields there will be a default value,
    If you enter '.', the field will be left blank.
    -----
    Common Name (eg: your user, host, or server name) [my-vpn-server-key]:

    Keypair and certificate request completed. Your files are:
    req: /home/centos/server/pki/reqs/my-vpn-server-key.req
    key: /home/centos/server/pki/private/my-vpn-server-key.key
    ```
  <article class="message is-warning">
  <div class="message-body">
  <strong>Warning</strong>
  <p>
   The nopass option means that the private key will not be encrypted. This is not recommended since unencrypted private keys can be used by anyone who obtain them. Encrypted keys offer stronger protection, but will require a passphrase on initial use.
  </p>
  </div>
  </article>

##### The client
1. On each client, setup the PKI and generate a keypair and request. Configure the PKI environment for the client:
       
    ```bash
    $ mkdir ~/client-01
    $ cd ~/client-01/
    $ /usr/share/easy-rsa/3/easyrsa init-pki
    ```
    ```
    init-pki complete; you may now create a CA or requests.
    Your newly created PKI dir is: /home/centos/client-01/pki
    ```
2. Generate the client keypair and request
        
    ```bash
    $ sudo /usr/share/easy-rsa/3/easyrsa gen-req client-01-key nopass
    ```
    ```
    Generating a 2048 bit RSA private key
    .+++
    .+++
    writing new private key to '/home/centos/client-01/pki/private/client-01-key.key.KdppWg4m9K'
    -----
    You are about to be asked to enter information that will be incorporated
    into your certificate request.
    What you are about to enter is what is called a Distinguished Name or a DN.
    There are quite a few fields but you can leave some blank
    For some fields there will be a default value,
    If you enter '.', the field will be left blank.
    -----
    Common Name (eg: your user, host, or server name) [client-01-key]:

    Keypair and certificate request completed. Your files are:
    req: /home/centos/client-01/pki/reqs/client-01-key.req
    key: /home/centos/client-01/pki/private/client-01-key.key
    ```
  
  <article class="message is-warning">
  <div class="message-body">
  <strong>Note</strong>
  <p>
   You should generate separate keys and certificates for each client you intend to connect to your VPN server. You shouldn't share keys and certificates with clients.
  </p>
  </div>
  </article>


#### Step Four - Generating Keys and Signing the Requests
In this step, we will import the certificate requests to the CA, where the CA signs and returns a valid certificate.

1. On the CA, import both the server and client request files.
  - The following commands will copy the specified files into the CA's reqs/ directory in preparation for signing.
     - Import the server request
         
            ```bash 
            $ cd ~/ca
            $ sudo /usr/share/easy-rsa/3/easyrsa import-req ~/server/pki/reqs/my-vpn-server-key.req vpn-server-01
            ```
            ```
            The request has been successfully imported with a short name of: vpn-server-01
            You may now use this name to perform signing operations on this request.
            ```
     - Import the client request
            
            ```bash
            $ sudo /usr/share/easy-rsa/3/easyrsa import-req ~/client-01/pki/reqs/client-01-key.req  client-01
            ```
            ```
            The request has been successfully imported with a short name of: client-01
            You may now use this name to perform signing operations on this request.
            ```

2. [Optional] Review each request's details.
        
      ```bash
      $ sudo /usr/share/easy-rsa/3/easyrsa show-req vpn-server-01
      ```
      ```
      Showing req details for 'vpn-server-01'.
      This file is stored at:
       /home/centos/ca/pki/reqs/vpn-server-01.req

    Certificate Request:
        Data:
            Version: 0 (0x0)
            Subject:
                commonName                = my-vpn-server-key
            Attributes:
                a0:00
     ```
     
     ```bash
      $ sudo /usr/share/easy-rsa/3/easyrsa show-req client-01
     ```
     ```
      Showing req details for 'client-01'.
      This file is stored at:
      /home/centos/ca/pki/reqs/client-01.req

      Certificate Request:
          Data:
              Version: 0 (0x0)
              Subject:
                  commonName                = client-01-key
              Attributes:
                  a0:00
     ```

3. Sign each request file as one of the types: _server_ or _client_
  - Sign the server's request file as a **server**. Type "yes" for confirmation and enter the passphrase.
        
        ```bash
         $ cd ~/ca
         $ sudo /usr/share/easy-rsa/3/easyrsa sign-req server vpn-server-01
        ```
        ```
        You are about to sign the following certificate.
        Please check over the details shown below for accuracy. Note that this request
        has not been cryptographically verified. Please be sure it came from a trusted
        source or that you have verified the request checksum with the sender.

        Request subject, to be signed as a server certificate for 3650 days:

        subject=
            commonName                = my-vpn-server-key


        Type the word 'yes' to continue, or any other input to abort.
          Confirm request details: yes
        Using configuration from /usr/share/easy-rsa/3/openssl-1.0.cnf
        Enter pass phrase for /home/centos/ca/pki/private/ca.key:
        Check that the request matches the signature
        Signature ok
        The Subject's Distinguished Name is as follows
        commonName            :ASN.1 12:'my-vpn-server-key'
        Certificate is to be certified until Oct 24 11:05:48 2028 GMT (3650 days)

        Write out database with 1 new entries
        Data Base Updated

        Certificate created at: /home/centos/ca/pki/issued/vpn-server-01.crt
        ```
  - Sign the client's request file as a **client**:
        
        ```bash
         $ sudo /usr/share/easy-rsa/3/easyrsa sign-req client client-01
        ```
        ```
        You are about to sign the following certificate.
        Please check over the details shown below for accuracy. Note that this request
        has not been cryptographically verified. Please be sure it came from a trusted
        source or that you have verified the request checksum with the sender.

        Request subject, to be signed as a client certificate for 3650 days:

        subject=
            commonName                = client-01-key


        Type the word 'yes' to continue, or any other input to abort.
          Confirm request details: yes
        Using configuration from /usr/share/easy-rsa/3/openssl-1.0.cnf
        Enter pass phrase for /home/centos/ca/pki/private/ca.key:
        Check that the request matches the signature
        Signature ok
        The Subject's Distinguished Name is as follows
        commonName            :ASN.1 12:'client-01-key'
        Certificate is to be certified until Oct 24 11:07:55 2028 GMT (3650 days)

        Write out database with 1 new entries
        Data Base Updated

        Certificate created at: /home/centos/ca/pki/issued/client-01.crt
        ``` 

4. Generates DH (Diffie-Hellman) parameters on the server
    - Generate the Diffie-Helllman parameters. This may take a few minutes to complete.
       
          ```bash
          $ cd ~/server
          $ sudo /usr/share/easy-rsa/3/easyrsa gen-dh
          ```
          ```
          Generating DH parameters, 2048 bit long safe prime, generator 2
          This is going to take a long time
          ....................................................+...............
          .................+..................................................
          ................................+...................................
          DH parameters of size 2048 created at /home/centos/server/pki/dh.pem
          ```



#### Step Five - Configuring OpenVPN

1. Create a directory to store the generated keys
      
      ```bash
      $ sudo mkdir -p /etc/openvpn/keys
      ```
2. Copy the generated keys to the OpenVPN config directory
      
      ```bash
      $ sudo cp ~/ca/pki/ca.crt /etc/openvpn/keys/
      $ sudo cp ~/server/pki/dh.pem /etc/openvpn/keys/
      $ sudo cp ~/server/pki/private/my-vpn-server-key.key /etc/openvpn/keys/
      $ sudo cp ~/ca/pki/issued/vpn-server-01.crt /etc/openvpn/keys/
      ```
3. Copy the example OpenVPN configuration file as a starting point for configuring our VPN server.

      ```bash
      $ sudo cp /usr/share/doc/openvpn-2.4.6/sample/sample-config-files/server.conf /etc/openvpn/
      ``` 
4. Open the configuration file in your text editor: `sudo nano /etc/openvpn/server.conf` and point to the files that contain the keys. Edit or uncomment (by removing the semicolon) the following lines:
      
      ```
      ca keys/ca.crt
      cert keys/vpn-server-01.crt
      key keys/my-vpn-server-key.key 
      dh keys/dh.pem

      topology subnet

      push "redirect-gateway def1 bypass-dhcp"
      push "dhcp-option DNS 1.1.1.1"
      push "dhcp-option DNS 1.0.0.1"

      log         /var/log/openvpn.log
      log-append  /var/log/openvpn.log

      user nobody
      group nobody
      ```
      - The above lines reference the generated keys and issued certificates. We also redirect all the client's network traffic through the VPN server and use [CloudFlare's public DNS resolver](https://www.cloudflare.com/learning/dns/what-is-1.1.1.1/). We also changed the default syslog log file into a specific file for OpenVPN.
5. Add a shared secret key to enhance the security of the VPN server.
  - Generate the shared secret key
        
        ```bash
        $ sudo openvpn --genkey --secret /etc/openvpn/keys/tls-crypt.key
        ```
  - Open `/etc/openvpn/server.conf` in your text editor and remove the line that starts with 
        
        ```
        tls-auth ta.key 0 # This file is secret
        ```
   and add:
        
        ```
        tls-crypt keys/tls-crypt.key 0
        ```

#### Step Six - Setting up Routing with iptables
Since we will set up a VPN server, we need to block most inbound connections, except over SSH and the OpenVPN port. We'll also want to allow incoming network traffic from VPN clients to the Internet. If we do not do that, VPN clients could only talk to our VPN server itself. Thus, we need to add firewall rules and routing configurations. 

1. 
  - Find the active zone
      
      ```bash
      $ sudo firewall-cmd --get-active-zones
      ```
      ```
      public
        interfaces: eth0
      ```
  - If the above command returns nothing, then you need to find your network interface using `$ ip address` and run the following commands:
      
      ```bash
      $ sudo firewall-cmd --permanent --zone=public --change-interface=eth0
      $ sudo firewall-cmd --reload
      $ sudo systemctl restart firewalld
      ```
2. Add OpenVPN to the list of services allowed by firewalld
      
      ```bash
      $ sudo firewall-cmd --zone=public --add-service openvpn
      $ sudo firewall-cmd --zone=public --add-port=1194/udp --permanent
      $ sudo firewall-cmd --zone=public --add-service openvpn --permanent
      $ sudo firewall-cmd --reload
      $ sudo systemctl restart firewalld
      ```
3. Next, enable [IP Masquerade](http://www.tldp.org/HOWTO/IP-Masquerade-HOWTO/ipmasq-background2.1.html)
      
      ```bash
      $ sudo firewall-cmd --permanent --add-masquerade
      $ sudo firewall-cmd --reload
      $ sudo systemctl restart firewalld
      ```
4. Next, forward routing to the OpenVPN subnet. We need to get the route to the DNS resolver we chose in the previous steps. We also need to use the VPN subnet that OpenVPN draws client addresses from (declared in _/etc/openvpn/server.conf_ as _server 10.8.0.0 255.255.255.0_).
      
      ```bash
      $ INTERFACE=$(ip route get 1.1.1.1 | awk 'NR==1 {print $(NF-2)}')
      $ sudo firewall-cmd --permanent --direct --passthrough ipv4 -t nat -A POSTROUTING -s 10.8.0.0/24 -o $INTERFACE -j MASQUERADE
      $ sudo firewall-cmd --reload
      ```
5. Next, enable IP forwarding. Open the file `/etc/sysctl.conf` in your text editor and add the following line at the top of the file:
     
     ```bash
     $ sudo nano /etc/sysctl.conf
     ```
     ```
     net.ipv4.ip_forward = 1
     ```
6. Finally, restart the network service to reflect these changes
    
    ```bash
    $ sudo systemctl restart network.service
    ```

#### Step Seven - Starting OpenVPN 
1. Setup OpenVPN to start automatically at boot
     
     ```bash
     $ sudo systemctl -f enable openvpn@server.service
     ```
2. Start the OpenVPN service:
     
     ```bash
     $ sudo systemctl start openvpn@server.service
     ```

#### Step Eight - Setting up the Client 
You need to have an OpenVPN client to connect to the OpenVPN server. Below is a list of some OpenVPN clients on common operating systems.

- Windows: 
  - [OpenVPN GUI](https://github.com/OpenVPN/openvpn-gui).
- macOS: 
  - [Tunnelblick](https://tunnelblick.net/).

1. Create a profile file for the client
    - `nano ~/client-01.ovpn`
    - Create the following OpenVPN client configuration file:

      ```
      client
      nobind
      dev tun
      remote-cert-tls server
      persist-key
      auth-nocache
      remote 34.207.77.21 1194 udp

      <key>

      </key>
      <cert>

      </cert>
      <ca>

      </ca>
      key-direction 1
      <tls-crypt>

      </tls-crypt>
      redirect-gateway def1
      ```
    - Copy and paste the content of the file `~/client-01/pki/private/client-01-key.key` inside the tag '<key>'.
    - Copy and paste the content of the file `~/ca/pki/issued/client-01.crt` inside the tag `<cert>` 
    - Copy and paste the content of the file `/etc/openvpn/keys/ca.crt ` inside the tag `<ca>`. 
    - Copy and paste the content of the file `/etc/openvpn/keys/tls-crypt.key ` inside the tag `<tls-crypt>`  tag.
    - Save the file. You should end up with something like:
      
         ```
          client
          nobind
          dev tun
          remote-cert-tls server

          remote 172.26.9.180 1194 udp

          <key>
    -----BEGIN PRIVATE KEY-----
    MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDgw/7lAOyl6le2
    .....    .....    .....    .....   .... .  .....   .....   .....
    PUIX4mrkcJviRiqW5Vzh/Z4sgqlMHckx2ZDHf2mI4BaGk2QQ587fOlUbg0TnFdlp
    -----END PRIVATE KEY-----
          </key>
          <cert>
          -----BEGIN CERTIFICATE-----
    MIIDZzCCAk+gAwIBAgIRAODTK0mT8iiHXpQmwiGVZeIwDQYJKoZIhvcNAQELBQAw
    .....    .....    .....    .....   .... .  .....   .....   .....
    EDEOMAwGA1UEAwwFbXl2cG4wHhcNMTgxMDI3MTEwNTQ4WhcNMjgxMDI0MTEwNTQ4
    -----END CERTIFICATE-----
          </cert>
          <ca>
          -----BEGIN CERTIFICATE-----
    MIIDITCCAgmgAwIBAgIJAOn8Fb9CWvnVMA0GCSqGSIb3DQEBCwUAMBAxDjAMBgNV
    .....    .....    .....    .....   .... .  .....   .....   .....
    BAMMBW15dnBuMB4XDTE4MTAyNzA1MDIxMVoXDTI4MTAyNDA1MDIxMVowEDEOMAwG
    -----END CERTIFICATE-----
          </ca>
          key-direction 1
          <tls-crypt>
          #
          # 2048 bit OpenVPN static key
          #
    -----BEGIN OpenVPN Static key V1-----
    cd741158706bca6ccbe105f4a916855f
    .....    .....    .....    .....
    7359257c94167046586710321ad7123b
    -----END OpenVPN Static key V1-----
          </tls-crypt>
          redirect-gateway def1
         ```

2. Download the OpenVPN profile file using `scp`
  - Open the Terminal on your client machine and download the file using _scp_:
     
     ```bash
     $ scp -i ~/path/to/ssh/key user-name@public-ip-address:~/client-01.ovpn ./
     ```
3. Import the `client-01.ovpn` file into your VPN client.
4. Connect to the VPN server from your VPN client.

#### Step Nine - Restrict Access to Your VM Instances to VPN Only
 - Log in to the dashboard of your cloud provider.
 - Find the option to limit access to your Virtual Private Network or instances to a range of IP addresses. Add the public IP address to restrict access to the VPN instance.
 - Create another VM instance and restrict SSH access to the public IP address of the VPN server instance.
 - Try to access the new instance without via the VPN and without using the VPN.
 - Clean up resources and delete the VM instances.


## Submission
Submit your answers with screenshots showing the commands you executed as a PDF file by the due date.