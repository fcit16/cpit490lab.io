---
title: "Lab 1"
date: 2018-09-01T14:12:36+03:00
draft: false
---
# Lab 1: Getting Started
> Install a Linux system, log in, and run simple commands using the shell.

<span class="tag is-info is-medium">Due on Thursday 20/9/2018 at 1:00PM</span>

## Objectives
1. Setting up a working Linux environment.
2. Logging in, activating the user interface, and logging out.
3. Changing user passwords
4. Reading manuals

## Setup
- Download and install Oracle VM [VirtualBox](https://www.virtualbox.org/), a free and open-source hypervisor.
- Download and install [CentOS (the minimal ISO)](https://www.centos.org/).


## Lab Exercises

1. Basic info
  - Obtain the following results
    - The name and version of the Operating System.
    - The logged in user name
    - The host name
2. Navigating the filesystem
  - Change directory `cd` to /root. What happens?
  - Change directory `cd` to your home directory and then `cd` to `..` . Use the `pwd` command. Where are you now?
3. Getting help
  - Read __man ls__.
      - How to list hidden files?
      - How to display the output in a long listing format and print directory size info in human readable format (e.g., KB, MB, GB)?
  - Read __man yum__. What are some commands you can use with __yum__?
  - The _a_ tool is particularly useful when searching for commands without knowing their exact names. apropos's syntax is: `apropos keywords`.
     - Search for _disk_ related tools using __apropos__. What is the name of the tool that gives you information about disk usage and space? If you have a problem, see the notes section below.
     - Use __apropos__ to find the editing programs that are available on your system? If you have a problem, see the notes section below.

## Notes
- If the command `apropos` returns _nothing appropriate_ on every search time you use or does not show recently installed software, then you'll have to run `mandb` as a root and run _apropos_ again.

## Submission
Submit your answers with screenshots showing the commands you executed as a PDF file by the due date.
